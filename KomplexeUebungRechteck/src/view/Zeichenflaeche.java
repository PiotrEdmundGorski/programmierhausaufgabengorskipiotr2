package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Iterator;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {
	
	private final BunteRechteckeController CONTROLLER;

	public Zeichenflaeche(BunteRechteckeController objekt) {
		CONTROLLER = objekt;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.BLUE);
		for (int i = 0; i < CONTROLLER.getRechtecke().size(); i++) {
			g.drawRect(CONTROLLER.getRechtecke().get(i).getX(), CONTROLLER.getRechtecke().get(i).getY(), CONTROLLER.getRechtecke().get(i).getHoehe(), CONTROLLER.getRechtecke().get(i).getBreite());
		}
	}
}