package controller;

import java.util.LinkedList;

import model.Rechteck;

public class BunteRechteckeController {

	LinkedList<Rechteck> rechtecke;

	public BunteRechteckeController() {
		rechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck rechteck) {
		this.rechtecke.add(rechteck);
	}

	public void reset() {
		this.rechtecke.clear();
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}

	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

}