package model;
import java.lang.Math;
public class Rechteck {

	private int y;
	private int x;
	private int breite = negativerBetrag(getBreite());
	private int hoehe = negativerBetrag(getHoehe());
	
	public Rechteck() {
	}
	
	public Rechteck(int y, int x, int breite, int hoehe) {
		this.y = y;
		this.x = x;
		this.breite = negativerBetrag(breite);
		this.hoehe = negativerBetrag(hoehe);
	}
	
	
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getBreite() {
		return breite;
	}
	public void setBreite(int breite) {
		this.breite = negativerBetrag(breite);
	}
	public int getHoehe() {
		return hoehe;
	}
	public void setHoehe(int hoehe) {
		this.hoehe = negativerBetrag(hoehe);
	}

	@Override
	public String toString() {
		return "Rechteck [y=" + y + ", x=" + x + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
	public int negativerBetrag (int zahl) {
		return Math.abs(zahl);
	}
	
	
}
