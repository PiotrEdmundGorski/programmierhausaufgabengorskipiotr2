package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {
	public static void main(String[] args) {

		Rechteck r0 = new Rechteck(10, 10, 30, 40);
		Rechteck r1 = new Rechteck(25, 25, 100, 20);
		Rechteck r2 = new Rechteck(260, 10, 200, 100);
		Rechteck r3 = new Rechteck(5, 500, 300, 25);
		Rechteck r4 = new Rechteck(100, 100, 100, 100);

		Rechteck r5 = new Rechteck();
		r5.setY(200);
		r5.setX(200);
		r5.setBreite(200);
		r5.setHoehe(200);
		Rechteck r6 = new Rechteck();
		r6.setY(800);
		r6.setX(400);
		r6.setBreite(20);
		r6.setHoehe(20);
		Rechteck r7 = new Rechteck();
		r7.setY(800);
		r7.setX(450);
		r7.setBreite(20);
		r7.setHoehe(20);
		Rechteck r8 = new Rechteck();
		r8.setY(850);
		r8.setX(400);
		r8.setBreite(20);
		r8.setHoehe(20);
		Rechteck r9 = new Rechteck();
		r9.setY(855);
		r9.setX(455);
		r9.setBreite(25);
		r9.setHoehe(25);
		
		BunteRechteckeController testObjekt = new BunteRechteckeController();
		testObjekt.add(r0);
		testObjekt.add(r1);
		testObjekt.add(r2);
		testObjekt.add(r3);
		testObjekt.add(r4);
		testObjekt.add(r5);
		testObjekt.add(r6);
		testObjekt.add(r7);
		testObjekt.add(r8);
		testObjekt.add(r9);
		System.out.println(testObjekt.toString());
		
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setX(-10);
	    eck11.setY(-10);
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
	}
	
}